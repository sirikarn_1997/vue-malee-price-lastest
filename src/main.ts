import Vue from "vue";
import App from "@/App.vue";

import { createApp, h } from "vue-demi";

import "windi.css";
import router from "@/router";
import Axios from "axios"

Vue.config.productionTip = false;
Vue.config.devtools = true;
const app = createApp({
  router,
  Axios,
 
  render: () => h(App),
});

app.mount("#app");
